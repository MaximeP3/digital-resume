const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
require('dotenv').config()

const app = express()

const uri = 'mongodb://' +
            process.env.DB_HOST + ':' +
            process.env.DB_PORT + '/' +
            process.env.DB_NAME

mongoose.connect(
  uri,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => console.log(`Successfully connected to MongoDB on ${uri}`))
  .catch(err => console.error('Connection to MongoDB failed !\n', err))

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
  next()
})

app.use(bodyParser.json())

app.get('/', (req, res, next) => {
  res.status(200).send('MaximeP3\'s digital-resume API')
})

module.exports = app